#include <zcross/ZCross.hpp>
#include <iostream>
#include <random>
#include <stdexcept>
#include <omp.h>
#include <boost/units/systems/si/codata/atomic-nuclear_constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/electron_constants.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/cmath.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>

#include <univec/VectorC3D.hpp>
#include <univec/Matrix3D.hpp>

using namespace dfpe;
using namespace std;
using namespace boost::units;
using namespace boost::units::si::constants::codata;

int main(int argc, char* argv[])
{
    if (argc != 6)
    {
        cout << "Usage: " << argv[0] << " <reduced field> <trial frequency> <temperature> <events> <interactions>" << endl;
        return 1;
    }

    QtySiArea        m2(1. * si::meter * si::meter);
    QtySiEnergy      eV(1. * si::volt * si::constants::codata::e);
    QtySiVelocity    cm_us(1. * si::centi * si::meter / (si::micro * si::second));
    QtySiTemperature k(1. * si::kelvin);

    /// derived dimension for electric field in SI units : L M T⁻³ A⁻¹
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension, boost::units::static_rational<1>>,
            boost::units::dim<boost::units::mass_base_dimension, boost::units::static_rational<1>>,
            boost::units::dim<boost::units::time_base_dimension, boost::units::static_rational<-3>>,
            boost::units::dim<boost::units::current_base_dimension, boost::units::static_rational<-1>>
    >>::type electric_field_dimension;

    /// derived dimension for electric field in SI units : L⁴ M T⁻³ A⁻¹
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension, boost::units::static_rational<4>>,
            boost::units::dim<boost::units::mass_base_dimension, boost::units::static_rational<1>>,
            boost::units::dim<boost::units::time_base_dimension, boost::units::static_rational<-3>>,
            boost::units::dim<boost::units::current_base_dimension, boost::units::static_rational<-1>>
    >>::type reduced_electric_field_dimension;

    typedef boost::units::unit<electric_field_dimension, boost::units::si::system>         electric_field;
    typedef boost::units::unit<reduced_electric_field_dimension, boost::units::si::system> reduced_electric_field;

    typedef quantity<electric_field>         QtySiElectricField;
    typedef quantity<reduced_electric_field> QtySiReducedElectricField;

    const QtySiElectricField             kV_cm(1. * si::kilo * si::volt / (si::centi * si::meter));
    const QtySiReducedElectricField      td(1e-21 * si::volt * si::meter * si::meter);
    const QtySiFrequency                 ghz(1. * si::giga * si::hertz);
    const auto                      kB = si::constants::codata::k_B;

    ZCross zcross;
    zcross.loadDatabase("Biagi11");

    ProcessFilter filter;
    filter.addFilterByDatabaseId("Biagi11");
    filter.addFilterByGroupId("Ar");
    filter.addFilterByProcessCollisionType(ProcessCollisionType::ELASTIC);

    cout << "Loading table:" << endl;
    const IntScatteringTable table = zcross.getIntScatteringTables(filter).at(0);

    cout << "[" << table.getId() << "]" << endl;
    cout << "  " << table.getProcess().getShortDescription() << endl;
    cout << "  " << table.getProcess().getReaction().toString(PrintMode::PRETTY) << endl;
    cout << "  " << "Comments: " << endl;
    cout << "  " << table.getProcess().getComments() << endl;
    cout << endl;

    cout << std::right
         << setw(16) << "ENERGY"
         << "   "
         << setw(16) << "CROSS SECTION"
         << endl;

    for (const auto &pair: table.getTable())
    {
        cout << std::right
             << setw(16) << (double) (pair.first / eV) << " eV"
             << setw(16) << (double) (pair.second / m2) << " m²"
             << endl;
    }
    cout << endl;

    // Values at 77 K
    // red field      trial  efficiency     events       stdev/mean
    // 0.001 Td ->   70 GHz    57.81 %
    // 0.002 Td ->   85 GHz    63.04 %        50            45.88 %
    // 0.005 Td ->  100 GHz    73.37 %        25            35.70 %
    // 0.01  Td ->  175 GHz    28.41 %        20            21.91 %
    // 0.02  Td ->  300 GHz    17.87 %        15             7.62 %
    // 0.05  Td ->  575 GHz    17.66 %        10            12.05 %
    // 0.1   Td -> 1000 GHz    17.70 %        10
    // 0.2   Td -> 1750 GHz    17.65 %        10
    // 0.5   Td -> 3500 GHz    18.48 %        10            13.48 %
    // 1.0   Td -> 7000 GHz    17.12 %        10            11.67 %

    int  events       = stoi(argv[4]);
    long interactions = stoul(argv[5]);

    QtySiDensity                         density = 2.6867811e25 / (si::meter * si::meter * si::meter);  // Loschmidt constant
    QtySiFrequency                       trialFrequency(stod(argv[2]) * si::giga * si::hertz);
    VectorC3D<QtySiReducedElectricField> reducedField(0. * td, 0. * td, stod(argv[1]) * td);
    VectorC3D<QtySiElectricField>        field   = reducedField * density;
    QtySiTemperature                     temperature(stod(argv[3]) * si::kelvin);

    QtySiMass           bulletMass   = m_e;
    QtySiMass           targetMass   = Molecule("Ar").getMass();
    QtySiMass           totalMass    = bulletMass + targetMass;
    QtySiElectricCharge bulletCharge = e;

    boost::accumulators::accumulator_set<double, boost::accumulators::features<boost::accumulators::tag::mean, boost::accumulators::tag::variance>> accVelocity;
    boost::accumulators::accumulator_set<double, boost::accumulators::features<boost::accumulators::tag::mean, boost::accumulators::tag::variance>> accEfficency;

#pragma omp parallel default(shared), firstprivate(table)
    {
        default_random_engine generator;
        unsigned int          now = std::chrono::system_clock::now().time_since_epoch().count();
        generator.seed(now + omp_get_thread_num());
        uniform_real_distribution<double> distribution(0.0, 1.0);
        gamma_distribution<double>        gamma(3. / 2., kB * temperature / eV);

#pragma omp for
        for (int eventId = 0; eventId < events; eventId++)
        {
#pragma omp critical
            cout << "Event " << eventId << endl;

            unsigned long realCollisions = 0.;
            unsigned long nullCollisions = 0.;
            unsigned long failCollisions = 0.;

            QtySiTime                currentTime;
            VectorC3D<QtySiLength>   labBulletPosition;
            VectorC3D<QtySiVelocity> labBulletVelocity;

            long currentInteraction = 0;
            while (currentInteraction < interactions)
            {
                QtySiDimensionless rnd1      = distribution(generator);
                QtySiTime          deltaTime = -log(1. - rnd1) / trialFrequency;

                VectorC3D<QtySiVelocity> labBulletNewVelocity = labBulletVelocity + bulletCharge * field / bulletMass * deltaTime;
                VectorC3D<QtySiLength>   labBulletNewPosition = labBulletPosition + labBulletVelocity * deltaTime + 0.5 * bulletCharge * field / bulletMass * deltaTime * deltaTime;

                QtySiArea      crossSection  = table.getInterpoledValue(0.5 * bulletMass * labBulletNewVelocity.normSquared(), OutOfTableMode::BOUNDARY_VALUE, OutOfTableMode::BOUNDARY_VALUE);
                QtySiFrequency realFrequency = labBulletNewVelocity.norm() * density * crossSection;

                if (trialFrequency >= realFrequency)
                {
                    quantity<si::dimensionless> ratioReal = realFrequency / trialFrequency;

                    labBulletVelocity = labBulletNewVelocity;
                    labBulletPosition = labBulletNewPosition;
                    currentTime += deltaTime;

                    quantity<si::dimensionless> rnd2 = distribution(generator);
                    if (rnd2 < ratioReal) // Checking if it is a not-null collision
                    {
                        QtySiVelocity labTargetVelocityNorm = sqrt(2. * gamma(generator) * eV / targetMass);

                        // Calculating LL frame
                        VectorC3D<QtySiVelocity> labTargetVelocity = VectorC3D<QtySiDimensionless>::unitaryRandom(generator) * labTargetVelocityNorm;

                        VectorC3D<QtySiDimensionless> labLlAxisZ = labBulletVelocity.versorDl();

                        VectorC3D<QtySiVelocity> labPerpendicular = labLlAxisZ.cross(labTargetVelocity);
                        if (labPerpendicular.isNull())
                            labPerpendicular = labLlAxisZ.cross(VectorC3D<QtySiVelocity>::fromX(QtySiVelocity::from_value(1.)));
                        if (labPerpendicular.isNull())
                            labPerpendicular = labLlAxisZ.cross(VectorC3D<QtySiVelocity>::fromY(QtySiVelocity::from_value(1.)));
                        if (labPerpendicular.isNull())
                            labPerpendicular = labLlAxisZ.cross(VectorC3D<QtySiVelocity>::fromZ(QtySiVelocity::from_value(1.)));
                        if (labPerpendicular.isNull())
                            assert(0);

                        VectorC3D<QtySiDimensionless> labLlAxisY = labPerpendicular.versorDl();
                        VectorC3D<QtySiDimensionless> labLlAxisX = labLlAxisZ.cross(labLlAxisY);

                        Matrix3D<QtySiDimensionless> llToLab(labLlAxisX, labLlAxisY, labLlAxisZ);
                        Matrix3D<QtySiDimensionless> labToLl = llToLab.inv();

                        VectorC3D<QtySiVelocity> llBulletVelocity = labToLl * labBulletVelocity;
                        VectorC3D<QtySiVelocity> llTargetVelocity = labToLl * labTargetVelocity;
                        VectorC3D<QtySiVelocity> llVelocityCom    = (bulletMass * llBulletVelocity + targetMass * llTargetVelocity) / totalMass;

                        QtySiPlaneAngle    llTargetTheta    = !llTargetVelocity.isNull() ? llTargetVelocity.angle(VectorC3D<QtySiDimensionless>::fromZ(-1.)) : QtySiPlaneAngle();
                        QtySiDimensionless llTargetCosTheta = cos(llTargetTheta);

                        QtySiVelocity delta = sqrt(llBulletVelocity.normSquared() + llTargetVelocity.normSquared() - 2. * llBulletVelocity.norm() * llTargetVelocity.norm() * llTargetCosTheta); // + something multipled by epsilon_k == 0
                        assert(isfinite(delta));

                        VectorC3D<QtySiVelocity> llBulletNewVelocity = llVelocityCom + targetMass / totalMass * delta * VectorC3D<QtySiDimensionless>::unitaryRandom(generator);
                        /*
                        {
                            VectorC3D<QtySiVelocity> comBulletVelocity = llBulletVelocity - llVelocityCom;
                            VectorC3D<QtySiVelocity> comTargetVelocity = llTargetVelocity - llVelocityCom;

                            QtySiDimensionless rnd3    = distribution(generator);
                            QtySiPlaneAngle    phi     = rnd3 * QtySiPlaneAngle(2. * M_PI * si::radians);
                            QtySiDimensionless rnd4    = distribution(generator);
                            QtySiDimensionless epsilon = 0.5 * bulletMass * comBulletVelocity.normSquared() / E_h;

                            QtySiPlaneAngle comBulletPostTheta = acos(QtySiDimensionless(1. - 2. * rnd4 / (1. + 8. * epsilon * (1. - rnd4))));
                            QtySiPlaneAngle comBulletPostPhi   = phi;

                            auto comPerpendicular = comBulletVelocity.cross(comTargetVelocity);
                            if (comPerpendicular.isNull())
                                comPerpendicular = comBulletVelocity.cross(VectorC3D<QtySiVelocity>::fromX(QtySiVelocity::from_value(1.)));
                            if (comPerpendicular.isNull())
                                comPerpendicular = comBulletVelocity.cross(VectorC3D<QtySiVelocity>::fromY(QtySiVelocity::from_value(1.)));
                            if (comPerpendicular.isNull())
                                comPerpendicular = comBulletVelocity.cross(VectorC3D<QtySiVelocity>::fromZ(QtySiVelocity::from_value(1.)));
                            if (comPerpendicular.isNull())
                                assert(0);

                            VectorC3D<QtySiDimensionless> comBulletPostDirection = comBulletVelocity.versorDl();
                            comBulletPostDirection = comBulletPostDirection.rotate(comBulletPostTheta, comPerpendicular);
                            comBulletPostDirection = comBulletPostDirection.rotate(comBulletPostPhi, comBulletVelocity);

                            llBulletNewVelocity = llVelocityCom + targetMass / totalMass * delta * comBulletPostDirection;

                            assert (llBulletNewVelocity.isFinite());

                        }
                         */

                        labBulletNewVelocity = llToLab * llBulletNewVelocity;
                        assert (labBulletNewVelocity.isFinite());
                        labBulletVelocity = labBulletNewVelocity;

                        currentInteraction++;
                        realCollisions += 1.;
                    }
                    else
                        nullCollisions += 1.;
                }
                else if (!isfinite(realFrequency))
                {
                    cout << "Trial frequency: " << trialFrequency << endl;
                    cout << "Real  frequency: " << realFrequency << endl;
                    throw runtime_error("Real frequency was an not finite number");
                }
                else
                {
                    failCollisions += 1.;
                    cout << "Trial frequency: " << trialFrequency << endl;
                    cout << "Real  frequency: " << realFrequency << endl;
                    throw runtime_error("Trial frequency was lower than the real frequency");
                }
            }

#pragma omp critical
            {
                accVelocity((double) (labBulletPosition.dot(field.versorDl()) / currentTime / cm_us));
                accVelocity((double) (labBulletPosition.dot(field.versorDl()) / currentTime / cm_us));
                accEfficency(double(realCollisions) / double(realCollisions + nullCollisions + failCollisions) * 100.);
            }
        }
    }

    QtySiVelocity      velocityMean   = boost::accumulators::mean(accVelocity) * cm_us;
    QtySiVelocity      velocityStdev  = sqrt(boost::accumulators::variance(accVelocity)) * cm_us;
    QtySiDimensionless efficiencyMean = boost::accumulators::mean(accEfficency);

    cout << left << "RESULTS:" << endl << "________________________________________" << endl
         << left << setw(30) << "run id" << right << std::setw(10) << 0 << "" << endl
         << left << setw(30) << "events" << right << std::setw(10) << events << "" << endl
         << left << setw(30) << "particles" << right << std::setw(10) << 1 << "" << endl
         << left << setw(30) << "interactions" << right << std::setw(10) << interactions << "" << endl
         << left << setw(30) << "temperature" << right << std::setw(10) << (double) (temperature / k) << " K" << endl
         << left << setw(30) << "field" << right << std::setw(10) << (double) (field.norm() / kV_cm) << " kV/cm" << endl
         << left << setw(30) << "reduced field" << right << std::setw(10) << (double) (reducedField.norm() / td) << " Td" << endl
         << left << setw(30) << "trial frequency"       << right << std::setw(10) << (double) (trialFrequency / ghz)        << " GHz" << endl
         << left << setw(30) << "drift velocity mean"   << right << std::setw(10) << (double) (velocityMean / cm_us)        << " cm/us" << endl
         << left << setw(30) << "drift velocity stdev"  << right << std::setw(10) << (double) (velocityStdev / cm_us)       << " cm/us" << endl
         << left << setw(30) << "drift velocity ratio"  << right << std::setw(10) << (double) (velocityStdev / velocityMean * 100.) << " %" << endl
         << left << setw(30) << "efficiency" << right   << std::setw(10) << (double) (efficiencyMean)                       << " %" << endl;
}