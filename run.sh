# Columns:
# 1: reduced field [Td]
# 2: trial frequency [GHz]
# 3: temperature [K]
# 4: events
# 5: real interactions per event


#        Td    GHz   K   
./thermo 0.001   85  77 1000 1000000 > result_A01
./thermo 0.002   85  77  500 1000000 > result_A02
./thermo 0.005  125  77  250 1000000 > result_A03
./thermo 0.01   200  77  200 1000000 > result_A04
./thermo 0.02   350  77  150 1000000 > result_A05
./thermo 0.05   600  77  100 1000000 > result_A06
./thermo 0.1   1000  77  100 1000000 > result_A07
./thermo 0.2   1750  77  100 1000000 > result_A08
./thermo 0.5   3500  77  100 1000000 > result_A09
./thermo 1     7000  77  100 1000000 > result_A10

#        Td     GHz   K   
./thermo 0.001  170 300 1000 1000000 > result_B01
./thermo 0.002  170 300  500 1000000 > result_B02
./thermo 0.005  250 300  250 1000000 > result_B03
./thermo 0.01   400 300  200 1000000 > result_B04
./thermo 0.02   700 300  150 1000000 > result_B05
./thermo 0.05  1200 300  100 1000000 > result_B06
./thermo 0.1   2000 300  100 1000000 > result_B07
./thermo 0.2   3500 300  100 1000000 > result_B08
./thermo 0.5   7000 300  100 1000000 > result_B09
./thermo 1    14000 300  100 1000000 > result_B10

#        Td     GHz   K   
./thermo 0.001  340 1000 5000 1000000 > result_C01
./thermo 0.002  340 1000  500 1000000 > result_C02
./thermo 0.005  500 1000  250 1000000 > result_C03
./thermo 0.01   800 1000  200 1000000 > result_C04
./thermo 0.02  1400 1000  150 1000000 > result_C05
./thermo 0.05  2400 1000  100 1000000 > result_C06
./thermo 0.1   4000 1000  100 1000000 > result_C07
./thermo 0.2   7000 1000  100 1000000 > result_C08
./thermo 0.5  14000 1000  100 1000000 > result_C09
./thermo 1    28000 1000  100 1000000 > result_C10
